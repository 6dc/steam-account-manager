#add here games you play in order to kill them too! 
function killgames {
  #csgo 
  get-process "csgo" -ErrorAction SilentlyContinue | kill
 
  #information where to add new games
  Write-Host "If your game was not killed, add it to the killgames function or ask the developer (me@6dc.eu)"
}

#look for steam path
$steamHome = (Get-ItemProperty -Path HKCU:\Software\Valve\Steam\ -Name SteamPath).SteamPath

#find steam executable
$steamdotexe = (Get-ItemProperty -Path HKCU:\Software\Valve\Steam\ -Name SteamExe).SteamExe

Write-Host "Info: Steam sucessfully found at '$steamHome'" -f Green

#Scan the steamname input
$steamname = Read-Host "Steam-account:" 

#find the appdata-location of steam
$appdatahome = Join-Path $steamHome "config\loginusers.vdf"
 
#process appdata
$appdata = Get-Content $appdatahome
$appdata = $appdata | % {$_ -replace '(\s+\"RememberPassword\"\s+)\"\S*(\".*$)','$1"1$2' }
$appdata = $appdata | % {$_ -replace '(\s+\"AutoLoginUser\"\s+\")\S*(\".*$)',('$1'+"$steamname"+'$2') }
$appdata | Set-Content $appdatahome

#stop steam
if( Get-Process steam -ErrorAction SilentlyContinue ) 
{ 
	Write-Host "Killing steam-games..."
	killgames
    Write-Host "Stopping steam..."
    & "$steamdotexe" -shutdown
    Wait-Process steam -ErrorAction SilentlyContinue
}

#start steam with the new account 
Write-Host "Starting Steam..."
Write-Host "Logging in as '$steamname'..."
& "$steamdotexe" -silent

